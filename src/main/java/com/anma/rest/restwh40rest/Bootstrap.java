package com.anma.rest.restwh40rest;

import com.anma.rest.restwh40rest.domain.*;
import com.anma.rest.restwh40rest.repositories.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

@Component
@Slf4j
public class Bootstrap implements CommandLineRunner {

    private final PlanetRepository planetRepository;
    private final RaceRepository raceRepository;
    private final MilitayForceRepository militayForceRepository;
    private final PrimarchRepository primarchRepository;
    private final AstartesRepository astartesRepository;

    public Bootstrap(PlanetRepository planetRepository,
                     RaceRepository raceRepository,
                     MilitayForceRepository militayForceRepository,
                     PrimarchRepository primarchRepository,
                     AstartesRepository astartesRepository) {
        this.planetRepository = planetRepository;
        this.raceRepository = raceRepository;
        this.militayForceRepository = militayForceRepository;
        this.primarchRepository = primarchRepository;
        this.astartesRepository = astartesRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        loadPlanets();
        loadRaces();
        loadMilitaryForces();
        loadAstartes();
        loadPrimarchs();
    }

    private void loadPlanets() {

        log.debug("================ Creating planets data.....");

        Planet cadia = Planet.builder()
                .id(UUID.randomUUID())
                .name("Cadia")
                .type(new String[] {"Civilised World", "Fortress World"})
                .population(new BigInteger("45643556850000000"))
                .system("Cadian System")
                .subSector("Cadian Sub-sector")
                .sector("Cadian Sector")
                .segmentum("Segmentum Obscurus")
//                .affiliation(new ArrayList<>(Arrays.asList(raceRepository.getRaceByName("Mankind"))))
                .build();

        planetRepository.save(cadia);
        log.debug("planet created");

        Planet terra = Planet.builder()
                .id(UUID.randomUUID())
                .name("Terra")
                .type(new String[]{"Hive World"})
                .population(new BigInteger("850000000"))
                .system("Sol System")
                .subSector("Sub-sector Solar")
                .sector("Sector Solar")
                .segmentum("Segmentum Solar")
                .build();
        planetRepository.save(terra);
        log.debug("planet created");

        Planet baal = Planet.builder()
                .id(UUID.randomUUID())
                .name("Baal")
                .type(new String[]{"Feral World (moons)", "Death World", "Space Marine Chapter Homeworld"})
                .population(new BigInteger("122000"))
                .system("Baal System")
                .subSector("Unknown")
                .sector("Stellar Grid: 01-HE0S/LS-14")
                .segmentum("Segmentum Ultima / Coreward")
                .orbitalRadius("3.4 AU")
                .build();
        planetRepository.save(baal);
        log.debug("planet created");

    }

    private void loadRaces() {

        Race mankind = Race.builder()
                .id(UUID.randomUUID())
                .name("Mankind")
                .capital("Terra")
                .planet(new ArrayList<Planet>(Arrays.asList(
                        planetRepository.getPlanetByName("Terra"),
                        planetRepository.getPlanetByName("Baal"))){
                })
                .build();

        raceRepository.save(mankind);

    }
    private void loadMilitaryForces() {

        MilitaryForce adeptusAstartes = MilitaryForce.builder()
                .name("Adeptus Astartes")
                .establishment("First Founding (30th Millennium)")
                .government("Imperium of Man")
                .headQuarters("Space Marine Chapter Homeworlds across the Milky Way Galaxy")
                .race(raceRepository.getRaceByName("Mankind"))
                .build();
        militayForceRepository.save(adeptusAstartes);
    }

    private void loadAstartes() {

        Astartes bloodAngels = Astartes.builder()
                .name("Blood Angels")
                .alegiance("Imperium of Man")
                .chapterMaster("Lord Commander Dante")
                .colours("Red and Black")
                .fortressMonastery("Arx Angelicum")
                .founding("First Founding (30th Millennium)")
                .homeWorld("Baal (Primary), Secondary tithe rights to the worlds of Carnae Verata, Atshen and Evernight (Protectorate)")
                .primarch(primarchRepository.getPrimarchByName("Sanguinius"))
                .warCry("For the Emperor and Sanguinius! Death! DEATH!")
                .number("IX")
                .successorsOf("The Revenant Legion")
                .build();

        Astartes greyKnights = Astartes.builder()
                .name("Grey Knights")
                .alegiance("Imperium of Man")
                .chapterMaster("Kaldor Draigo, Supreme Grand Master of the Grey Knights")
                .colours("Steel Grey and Red")
                .fortressMonastery("Citadel of Titan")
                .founding("During the Horus Heresy (31st Millennium)")
                .homeWorld("Titan")
//                .primarch()
                .warCry("I am the hammer, I am the right hand of the Emperor, the instrument of His will, " +
                                "the gauntlet about His fist, the tip of His spear, the edge of His sword!")
                .number("666 (Chapter Designation); Approx. 1,183 Astartes in the Chapter along with 32 Neophytes and 1,005 recruits in 999.M41")
                .successorsOf("none")
                .build();

        astartesRepository.save(bloodAngels);
        astartesRepository.save(greyKnights);

    }
    private void loadPrimarchs() {

        Primarch sanguinius = Primarch.builder()
                .name("Sanguinius")
                .astartes(astartesRepository.getAstartesByName("Blood Angels"))
                .build();
        primarchRepository.save(sanguinius);

    }
}
