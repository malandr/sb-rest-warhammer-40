package com.anma.rest.restwh40rest.api.v1.controllers;

import com.anma.rest.restwh40rest.domain.Astartes;
import com.anma.rest.restwh40rest.repositories.AstartesRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AstartesController.BASE_URL)
public class AstartesController {

    final static String BASE_URL = "/api/v1/astartes";

    private final AstartesRepository astartesRepository;

    public AstartesController(AstartesRepository astartesRepository) {
        this.astartesRepository = astartesRepository;
    }

    @GetMapping()
    public ResponseEntity<Iterable<Astartes>> getRaces() {

        return new ResponseEntity<>(astartesRepository.findAll(), HttpStatus.OK);
    }
}
