package com.anma.rest.restwh40rest.api.v1.controllers;

import com.anma.rest.restwh40rest.domain.Planet;
import com.anma.rest.restwh40rest.domain.Race;
import com.anma.rest.restwh40rest.repositories.RaceRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(RaceController.BASE_URL)
public class RaceController {

    final static String BASE_URL = "/api/v1/race";

    private final RaceRepository raceRepository;

    public RaceController(RaceRepository raceRepository) {
        this.raceRepository = raceRepository;
    }

    @GetMapping()
    public ResponseEntity<Iterable<Race>> getRaces() {

        return new ResponseEntity<>(raceRepository.findAll(), HttpStatus.OK);
    }
}
