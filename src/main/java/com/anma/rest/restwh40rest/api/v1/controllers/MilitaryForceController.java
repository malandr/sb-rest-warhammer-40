package com.anma.rest.restwh40rest.api.v1.controllers;

import com.anma.rest.restwh40rest.domain.MilitaryForce;
import com.anma.rest.restwh40rest.repositories.MilitayForceRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(MilitaryForceController.BASE_URL)
public class MilitaryForceController {

    final static String BASE_URL = "/api/v1/militaryForce";

    private final MilitayForceRepository militayForceRepository;

    public MilitaryForceController(MilitayForceRepository militayForceRepository) {
        this.militayForceRepository = militayForceRepository;
    }

    @GetMapping
    public ResponseEntity<Iterable<MilitaryForce>> getForces() {

        return new ResponseEntity<>(militayForceRepository.findAll(), HttpStatus.OK);
    }
}
