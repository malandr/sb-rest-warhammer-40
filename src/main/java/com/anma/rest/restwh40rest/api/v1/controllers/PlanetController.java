package com.anma.rest.restwh40rest.api.v1.controllers;


import com.anma.rest.restwh40rest.domain.Planet;
import com.anma.rest.restwh40rest.repositories.PlanetRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(PlanetController.BASE_URL)
public class PlanetController {

    final static String BASE_URL = "/api/v1/planet";

    private final PlanetRepository planetRepository;

    public PlanetController(PlanetRepository planetRepository) {
        this.planetRepository = planetRepository;
    }

    @GetMapping()
    public ResponseEntity<Iterable<Planet>> getPlanets() {

        return new ResponseEntity<>(planetRepository.findAll(), HttpStatus.OK);
    }

//    @GetMapping("/{planetId}")
//    public ResponseEntity<Planet> getPlanetById(@PathVariable UUID planetId) {
//
//        return new ResponseEntity<>(planetRepository.findById(planetId).get(), HttpStatus.OK);
//    }

    @GetMapping("/{planetName}")
    public ResponseEntity<Planet> getPlanetByName(@PathVariable String planetName) {

        return new ResponseEntity<>(planetRepository.getPlanetByName(planetName), HttpStatus.OK);
    }



}
