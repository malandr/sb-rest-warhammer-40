package com.anma.rest.restwh40rest.repositories;

import com.anma.rest.restwh40rest.domain.Primarch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PrimarchRepository extends JpaRepository<Primarch, UUID> {

    Primarch getPrimarchByName(String name);
}
