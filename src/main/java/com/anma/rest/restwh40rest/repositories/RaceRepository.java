package com.anma.rest.restwh40rest.repositories;

import com.anma.rest.restwh40rest.domain.Race;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RaceRepository extends JpaRepository<Race, UUID> {

    Race getRaceByName(String name);
}
