package com.anma.rest.restwh40rest.repositories;

import com.anma.rest.restwh40rest.domain.Planet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PlanetRepository extends JpaRepository<Planet, UUID> {

    Planet getPlanetByName(String name);
}
