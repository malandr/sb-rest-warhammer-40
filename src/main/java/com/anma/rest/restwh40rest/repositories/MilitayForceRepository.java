package com.anma.rest.restwh40rest.repositories;

import com.anma.rest.restwh40rest.domain.MilitaryForce;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MilitayForceRepository extends JpaRepository<MilitaryForce, UUID> {

    MilitaryForce getMilitaryForceByName(String name);
}
