package com.anma.rest.restwh40rest.repositories;

import com.anma.rest.restwh40rest.domain.Astartes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AstartesRepository extends JpaRepository<Astartes, UUID> {

    Astartes getAstartesByName(String name);
}
