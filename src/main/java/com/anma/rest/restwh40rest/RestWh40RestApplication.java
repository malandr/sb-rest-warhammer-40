package com.anma.rest.restwh40rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestWh40RestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestWh40RestApplication.class, args);
	}

}
