package com.anma.rest.restwh40rest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//@Table(name = "races")
public class Race {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type="org.hibernate.type.UUIDCharType")
    @Column(length = 36, columnDefinition = "varchar(36)", updatable = false, nullable = false, name = "race_id")
    private UUID id;

    private String name;

//    @JsonIgnore
//    @ManyToOne(fetch = FetchType.EAGER, optional = true)
//    @JoinColumn(name = "planet_id", referencedColumnName = "planet_id", nullable = true)
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    private Planet planet;

    @JsonIgnore
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "race_planet",
            joinColumns = { @JoinColumn(name = "race_id") },
            inverseJoinColumns = { @JoinColumn(name = "planet_id") }
    )
    private List<Planet> planet;

    private String capital;

    @OneToMany(mappedBy = "race")
    private List<MilitaryForce> militaryForce;
//    private List<MajorSpecies> majorSpecies;




}
