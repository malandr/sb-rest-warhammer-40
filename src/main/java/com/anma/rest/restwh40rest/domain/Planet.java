package com.anma.rest.restwh40rest.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Planet {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type="org.hibernate.type.UUIDCharType")
    @Column(length = 36, columnDefinition = "varchar(36)", updatable = false, nullable = false, name = "planet_id")
    private UUID id;
    private String name;

    @ManyToMany(mappedBy = "planet")
    private List<Race> affiliation;

//    @OneToMany(mappedBy = "planet")
//    private List<Race> affiliation;

    private BigInteger population;
    private String system;
    private String orbitalRadius;
    private String[] type;
    private String subSector;
    private String sector;
    private String segmentum;

//    @JsonIgnore
//    private String titheGrade;
}
