package com.anma.rest.restwh40rest.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Astartes {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type="org.hibernate.type.UUIDCharType")
    @Column(length = 36, columnDefinition = "varchar(36)", updatable = false, nullable = false, name = "astartes_id")
    private UUID id;

    private String name;
    private String warCry;
    private String founding;
    private String successorsOf;
//    private String successorChapters;
    private String number;

    @OneToOne(mappedBy = "astartes")
    private Primarch primarch;

    private String chapterMaster;
    private String homeWorld;
    private String fortressMonastery;
    private String alegiance;
    private String colours;

}
